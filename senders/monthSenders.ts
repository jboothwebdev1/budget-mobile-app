import useSWR from "swr"
import { IMonth } from "../interfaces/IMonth"
import { IMonthsReturn } from "../interfaces/returnObjects/IMonthsReturn"
import { api } from "../utils/api/api"
import { fetcher } from "../utils/fetcher/fetcher"
import { defaultHeaders } from "../utils/headers"

export function useGetAllMonths(userId: string): IMonthsReturn {
  const { data, error } = useSWR<Array<IMonth>>(
    `${api}/month/${userId}`,
    fetcher,
  )
  return {
    months: data,
    monthsLoading: !data && !error,
    monthsError: error,
  }
}

export async function createMonth(item: IMonth) {
  const request = await fetch(`${api}/month`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
    body: JSON.stringify(item),
  })
  return request
}

export async function updateMonth(item: IMonth) {
  const request = await fetch(`${api}/month`, {
    method: "PATCH",
    headers: defaultHeaders,
    body: JSON.stringify(item),
  })
  return request
}

export async function deleteMonth(item: IMonth) {
  const request = await fetch(`${api}/month`, {
    method: "DELETE",
    headers: defaultHeaders,
    body: JSON.stringify(item),
  })
  return request
}
