import { useEffect } from "react"
import { useUserId } from "../../state/useUserId"
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import Month from "./Month"
import Categories from "./Categories"
import Transactions from "./Transactions"
import MonthMenu from "../menus/MonthMenu"
import { AntDesign, MaterialCommunityIcons } from "@expo/vector-icons"
import { MONTHS } from "../../utils/months/months"

const Tab = createBottomTabNavigator()

export default function Budget({ user }: any) {
  const { userId, setUserId } = useUserId()

  useEffect(() => {
    const splitId = user.sub.split("|")
    setUserId(splitId[1])
  })

  const currentMonth = new Date(Date.now()).getMonth()
  const formattedMonth = {
    name: `${MONTHS[currentMonth].index} - ${MONTHS[currentMonth].name}`,
  }

  return (
    <Tab.Navigator initialRouteName="Month">
      <Tab.Screen
        name={"Month"}
        options={{
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons
              name="calendar-month-outline"
              size={24}
              color="white"
            />
          ),
          headerRight: props => <MonthMenu userId={userId} />,
        }}
      >
        {props => (
          <Month
            {...props}
            userId={userId}
          />
        )}
      </Tab.Screen>
      <Tab.Screen
        name="Categories"
        options={{
          tabBarIcon: ({ color, size }) => (
            <AntDesign
              name="bars"
              size={24}
              color="white"
            />
          ),
          headerRight: props => <MonthMenu userId={userId} />,
        }}
      >
        {props => (
          <Categories
            {...props}
            userId={userId}
          />
        )}
      </Tab.Screen>
      <Tab.Screen
        name="Transactions"
        options={{
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons
              name="cash"
              size={24}
              color="white"
            />
          ),
          headerRight: props => <MonthMenu userId={userId} />,
        }}
      >
        {props => (
          <Transactions
            {...props}
            month={formattedMonth}
            userId={userId}
          />
        )}
      </Tab.Screen>
    </Tab.Navigator>
  )
}
