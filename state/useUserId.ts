import useSwr from "swr"
import { IUser } from "../interfaces/IUser"

const initial: string = ""

export const useUserId = () => {
  const { data, error, mutate } = useSwr("getUserId")
  const userId = data || initial
  const userLoading = !data
  const setUserId = mutate


  return {
    userId,
    userLoading,
    error,
    setUserId
  }
}

