import useSWR from "swr"
import {ITransactions} from "../interfaces/ITransactions"
import {api} from "../utils/api/api"
import {fetcher} from "../utils/fetcher/fetcher"
import {defaultHeaders} from "../utils/headers"

export function useGetAllTransactions(userId: string) {
  const { data, error } = useSWR<Array<ITransactions>>(
    `${api}/transaction/${userId}`,
    fetcher,
  )
  return {
    transactions: data || [],
    transactionLoading: !data && !error,
    transactionError: error,
  }
}

export function useGetTransactionsByMonth(month: any, userId: string) {
  const { data, error, mutate } = useSWR<Array<ITransactions>>(
    `${api}/transaction/month/${month.name}/${userId}`,
    fetcher,
  )

  return {
    transactions: data || [],
    transactionsLoading: !data && !error,
    updateTransactions: mutate,
    transactionError: error,
  }
}

export async function createTransactions(item: ITransactions) {
  const request = await fetch(`${api}/transaction`, {
    method: "POST",
    headers: defaultHeaders,
    body: JSON.stringify(item),
  })
  return request
}

export async function deleteTransaction(item: ITransactions) {
  const request = await fetch(`${api}/transaction`, {
    method: "DELETE",
    headers: defaultHeaders,
    body: JSON.stringify(item),
  })
  return request
}

export async function updateTransaction(item: ITransactions) {
  const request = await fetch(`${api}/transaction`, {
    method: "PATCH",
    headers: defaultHeaders,
    body: JSON.stringify(item),
  })
}
