import { Alert, View } from "react-native"
import { Button, Menu, Text, TextInput } from "react-native-paper"
import { useState } from "react"
import { useGetAllCategories } from "../../senders/categoriesSenders"
import { useUserId } from "../../state/useUserId"
import useEditTransactionForm from "../../hooks/forms/useEditTransactionForm"
import { ITransactions } from "../../interfaces/ITransactions"
import {updateTransaction, useGetTransactionsByMonth} from "../../senders/transactionSenders"
import {formStyles} from "../../styles/formStyles";
import {useNavigation} from "@react-navigation/native";

const currentMonth = new Date(Date.now()).getMonth()

export default function EditTransactionForm({ route }: any) {
  const  transaction: ITransactions = route.params.transaction
  const update = route.params.update
  const navigation = useNavigation()
  const { userId } = useUserId()
  const [visible, setVisible] = useState<boolean>(false)
  const { categories, categoriesLoading } = useGetAllCategories(userId)
  const { values, handleChange } = useEditTransactionForm(transaction)

  const openMenu = () => setVisible(true)

  const closeMenu = () => setVisible(false)

  const onSubmit = () => {
    const body: ITransactions = {
      _id: transaction._id,
      category: values.category,
      month: transaction.month,
      spent: parseFloat(values.spent.toString()),
      userId: transaction.userId,
      year: transaction.year,
    }
    try {
      updateTransaction(body).then(() => {
        Alert.alert("Success", "Transaction update successfully")
        update().then(() => {
          // @ts-ignore
          navigation.reset({index: 0, routes: [{name: "Transactions"}]});
          // @ts-ignore
          navigation.navigate("Transactions")
        })
      })
    } catch (e: any) {
      console.warn(e.message, e.stack)
    }
  }

  return (
    <View style={formStyles.formContainer}>
      <Text style={formStyles.formTitle}>
        Create new transaction
      </Text>
      <View style={{ paddingHorizontal: 50, marginTop: 20 }}>
        <Menu
          style={formStyles.formInput}
          visible={visible}
          onDismiss={closeMenu}
          anchor={
            <Button
              mode={"contained"}
              color={"#bbb"}
              onPress={openMenu}
            >
              Select Category
            </Button>
          }
        >
          {categories.map(category => {
            return (
              <Menu.Item
                key={category._id}
                title={category.name}
                onPress={() => handleChange("category", category.name)}
              />
            )
          })}
        </Menu>
      </View>
      <TextInput
        style={formStyles.formInput}
        label="Amount spent"
        mode="flat"
        keyboardType="numeric"
        onChangeText={input => handleChange("spent", input)}
        value={values.spent.toString()}
      />
      <Text
        style={formStyles.formTitle}
      >
        Transaction preview:
      </Text>
      <Text
        style={formStyles.formPreview}
      >
        Category: {values.category}
      </Text>
      <Text
        style={formStyles.formPreview}
      >
        {" "}
        Spent: ${values.spent}
      </Text>
      <Button
        style={formStyles.formButton}
        mode="outlined"
        onPress={onSubmit}
      >
        Submit
      </Button>
    </View>
  )
}
