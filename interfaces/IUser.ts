export interface IUser {
  aud: string,
  email: string,
  email_verified: boolean,
  exp: number,
  family_name: string,
  given_name: string,
  iat: number,
  iss: string,
  locale: string,
  name: string,
  nickname: string,
}

export const initialUser =
{
  aud: "",
  email: '',
  email_verified: false,
  exp: 0,
  family_name: "",
  given_name: "",
  iat: 0,
  iss: "",
  locale: "",
  name: "",
  nickname: "",

}
