import {Alert, View} from "react-native";
import useEditCategoryForm from "../../hooks/forms/useEditCategoryForm";
import {Button, TextInput, Text} from "react-native-paper";
import {ICategory} from "../../interfaces/returnObjects/ICategory";
import { updateCategory} from "../../senders/categoriesSenders";
import {formStyles} from "../../styles/formStyles";
import {useNavigation} from "@react-navigation/native";

export default function EditCategoryForm({route}: any){
  const {category} = route.params
  const update = route.params.update
  const {values, handleChange} = useEditCategoryForm(category)
  const navigation = useNavigation()

  const onSubmit = () =>  {
    const body: ICategory = {
      _id: category._id,
      name: values.name.toLowerCase(),
      amount: values.amount,
      currentMonth: category.currentMonth,
      spent: values.spent,
      userId: category.userId,
      due: values.due?.toLowerCase(),
      frequency: values.frequency?.toLowerCase()
    }

    try {
      updateCategory(body)
        .then(() => {
          Alert.alert("Success", "Category updated successfully")
          update().then(() => {
            // @ts-ignore
            navigation.reset({index: 0, routes: [{name: "Categories"}]})
            // @ts-ignore
            navigation.navigate("Categories")
          })
        }).catch(() => {
          return new Error("Category updated unsuccessful")
      })
    } catch (e: any){
      console.info(e.message, e.stack)
    }
  }

  return (
    <View
    style={
      formStyles.formContainer
    }
    >
      <Text style={
        formStyles.formTitle
      }>
        Edit Category
      </Text>

      <TextInput
        style = {
        formStyles.formInput
        }
        label="Name"
        mode="flat"
        onChangeText={input => handleChange("name", input) }
        value={values.name}
      />

      <TextInput
        style = {
          formStyles.formInput
        }
        label="Amount"
        mode="flat"
        keyboardType={"numeric"}
        onChangeText={input => handleChange("amount", input)}
        value={values.amount.toString()}
        />
      <TextInput
        style = {
          formStyles.formInput
        }
        label="Spent"
        mode="flat"
        keyboardType={"numeric"}
        onChangeText={input => handleChange("spent", input)}
        value={values.spent.toString()}
      />
      <TextInput
        style = {
          formStyles.formInput
        }
        label="Due"
        mode="flat"
        onChangeText={input => handleChange("due", input)}
        value={values.due}
      />
      <TextInput
        style = {
          formStyles.formInput
        }
        label="Frequency"
        mode="flat"
        onChangeText={input => handleChange("frequency", input)}
        value={values.frequency}
        />

      <Text style={formStyles.formTitle}>
        Category preview:
      </Text>

      <Text style={formStyles.formPreview}>
        Name: {values.name}
      </Text>

      <Text style={formStyles.formPreview}>
        Amount: ${values.amount}
      </Text>

      <Text style={formStyles.formPreview}>
        Spent: ${values.spent}
      </Text>
      <Text style={formStyles.formPreview}>
        Due: {values.due}
      </Text>
      <Text style={formStyles.formPreview}>
        Frequency: {values.frequency}
      </Text>

      <Button
        mode="outlined"
        style={formStyles.formButton}
        onPress={onSubmit}
       >Submit</Button>
    </View>
  )
}