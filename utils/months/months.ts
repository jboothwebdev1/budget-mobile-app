export const MONTHS = [
  {
    name: "January",
    index: "01",
  },
  {
    name: "February",
    index: "02",
  },

  {
    name: "March",
    index: "03",
  },
  {
    name: "April",
    index: "04",
  },
  {
    name: "May",
    index: "05",
  },
  {
    name: "June",
    index: "06",
  },
  {
    name: "July",
    index: "07",
  },
  {
    name: "August",
    index: "08",
  },
  {
    name: "September",
    index: "09",
  },
  {
    name: "October",
    index: "10",
  },
  {
    name: "November",
    index: "11",
  },
  {
    name: "December",
    index: "12",
  },
]
