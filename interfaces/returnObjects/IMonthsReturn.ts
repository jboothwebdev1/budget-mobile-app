import { IMonth } from "../IMonth";

export interface IMonthsReturn {
  months: Array<IMonth> | undefined,
  monthsLoading: boolean,
  monthsError: any | undefined
}
