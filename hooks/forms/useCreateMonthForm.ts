import { BaseSyntheticEvent, useState } from "react"
import { IMonth } from "../../interfaces/IMonth"

const initialState: IMonth = {
  _id: "",
  expenses: 0,
  income: 0,
  name: "",
  userId: "",
  year: 0,
}

export default function useCreateMonthForm(callback: Function) {
  const [values, setValues] = useState<IMonth>(initialState)

  const handleChange = (event: BaseSyntheticEvent) => {
    event.persist()
    setValues((values: IMonth) => ({
      ...values,
      [event.target.name]: event.target.value,
    }))
  }
  const handleSubmit = (event: BaseSyntheticEvent) => {
    event.preventDefault()
    callback()
  }

  return {
    values,
    handleChange,
    handleSubmit,
  }
}
