import { View } from "react-native"
import { useState } from "react"
import { Button, Menu } from "react-native-paper"

type propType = {
  setYear: Function
}

export default function YearSelectionMenu({ setYear }: propType) {
  const [visible, setVisible] = useState(false)
  const openMenu = () => setVisible(true)
  const closeMenu = () => setVisible(false)

  const years: Array<number> = []
  for (let i = 2020; i <= 2030; i++) {
    years.push(i)
  }

  return (
    <View style={{ paddingHorizontal: 50 }}>
      <Menu
        visible={visible}
        onDismiss={closeMenu}
        anchor={
          <Button
            mode="contained"
            color="#bbb"
            onPress={openMenu}
          >
            Select year
          </Button>
        }
      >
        {years.map(year => {
          return (
            <Menu.Item
              key={year}
              title={year}
              onPress={() => {setYear(year); closeMenu()}}
            />
          )
        })}
      </Menu>
    </View>
  )
}
