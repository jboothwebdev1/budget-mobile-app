import { useUserId } from "../../../state/useUserId"
import useCreateTransactionForm from "../../../hooks/forms/useCreateTransactionForm"
import { Alert, View } from "react-native"
import { Button, Menu, Text, TextInput } from "react-native-paper"
import { useState } from "react"
import { useGetAllCategories } from "../../../senders/categoriesSenders"
import MonthSelectionMenu from "../../menus/MonthSelectionMenu"
import { ITransactions } from "../../../interfaces/ITransactions"
import { createTransactions } from "../../../senders/transactionSenders"
import {useNavigation} from "@react-navigation/native";

export default function CreateTransaction() {
  const { userId } = useUserId()
  const { categories, categoriesLoading } = useGetAllCategories(userId)
  const [month, setMonth] = useState<string>("")
  const [visible, setVisible] = useState<boolean>(false)
  const { values, handleChange } = useCreateTransactionForm()
  const navigation = useNavigation()
  const currentYear = new Date(Date.now()).getFullYear()

  const openMenu = () => setVisible(true)

  const closeMenu = () => setVisible(false)

  const onSubmit = () => {
    const body: ITransactions = {
      category: values.category,
      month: month,
      spent: parseFloat(values.spent.toString()),
      userId: userId,
      year: currentYear,
    }
    console.log(body)
    try {
      createTransactions(body)
        .then(() => {
          Alert.alert("Success", "Transaction created successfully")
          // @ts-ignore
          navigation.reset({index: 0 , routes: [{name: "Transactions"}]})
          // @ts-ignore
          navigation.navigate("Transactions")
        })
    } catch (e: any) {
      console.info(e.message, e.stack)
    }
  }

  return (
    <View>
      <Text style={{ color: "#D7D0CF", marginTop: 10, fontSize: 20 }}>
        Create new transaction
      </Text>
      <View style={{ paddingHorizontal: 50, marginTop: 20 }}>
        <Menu
          visible={visible}
          onDismiss={closeMenu}
          anchor={
            <Button
              mode={"contained"}
              color={"#bbb"}
              onPress={openMenu}
            >
              Select Category
            </Button>
          }
        >
          {categories.map(category => {
            return (
              <Menu.Item
                key={category._id}
                title={category.name}
                onPress={() => {handleChange("category", category.name); closeMenu()}}
              />
            )
          })}
        </Menu>
      </View>
      <MonthSelectionMenu setMonth={setMonth} />
      <TextInput
        style={{ marginHorizontal: 50 }}
        label="Amount spent"
        mode="flat"
        keyboardType="numeric"
        onChangeText={input => handleChange("spent", input)}
        value={values.spent.toString()}
      />
      <Text
        style={{
          fontSize: 20,
          marginVertical: 20,
          paddingHorizontal: 50,
        }}
      >
        Transaction preview:
      </Text>
      <Text
        style={{
          fontSize: 20,
          paddingHorizontal: 70,
        }}
      >
        Category: {values.category}
      </Text>
      <Text
        style={{
          fontSize: 20,
          marginTop: 10,
          paddingHorizontal: 70,
        }}
      >
        {" "}
        Month: {month}
      </Text>
      <Text
        style={{
          fontSize: 20,
          marginTop: 10,
          paddingHorizontal: 70,
        }}
      >
        {" "}
        Spent: {values.spent}
      </Text>
      <Button
        style={{ marginHorizontal: 50, marginTop: 20 }}
        mode="outlined"
        onPress={onSubmit}
      >
        Submit
      </Button>
    </View>
  )
}
