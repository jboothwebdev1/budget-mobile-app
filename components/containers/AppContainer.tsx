import { useGetAllMonths } from "../../senders/monthSenders"
import { Text, View } from "react-native"
import MonthCardView from "../monthComponents/MonthCardView"
import Loading from "../utils/Loading"

export default function AppContainer({ userId }: any) {
  const { months, monthsLoading } = useGetAllMonths(userId)

  return (
    <View>
      {monthsLoading ? <Loading /> : <MonthCardView months={months} />}
    </View>
  )
}
