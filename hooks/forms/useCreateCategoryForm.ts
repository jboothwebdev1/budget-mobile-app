import { useState } from "react"
import { ICategory } from "../../interfaces/returnObjects/ICategory"

const initialState: ICategory = {
  _id: "",
  amount: 0,
  currentMonth: 0,
  name: "",
  spent: 0,
  userId: "",
  due: "",
  frequency: "",
}

export default function useCreateCategoryForm() {
  const [values, setValues] = useState<ICategory>(initialState)

  const handleChange = (key: string, value: string | number) => {
    setValues((values: ICategory) => ({ ...values, [key]: value }))
  }

  return {
    values,
    handleChange,
  }
}
