import MonthCard from "./MonthCard"
import { Animated, FlatList, SectionList, View } from "react-native"

export default function MonthCardView({ months, navigation }: any) {
  // Take in the months from the app and map them to the card
  return (
    <View>
      <FlatList
        data={months}
        renderItem={({ item }) => <MonthCard month={item} />}
      ></FlatList>
    </View>
  )
}
