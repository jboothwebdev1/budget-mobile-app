module.exports ={
    trailingComma: "all",
    printWidth: 80,
    tabWidth: 2,
    arrowParens: "avoid",
    semi: false,
    singleQuote: false,
    singleAttributePerLine: true
}
