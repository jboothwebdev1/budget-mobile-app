import {useState} from "react"
import {ITransactions} from "../../interfaces/ITransactions"

type editTransaction = {
  category: string
  spent: number
}

export default function useEditTransactionForm(initialState: ITransactions) {
  const [values, setValues] = useState<editTransaction>(initialState)
  const handleChange = (key: string, value: string | number) => {
    setValues(values => ({ ...values, [key]: value }))
  }
  return {
    values,
    handleChange,
  }
}
