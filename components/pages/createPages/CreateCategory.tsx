import useCreateCategoryForm from "../../../hooks/forms/useCreateCategoryForm"
import { Alert, View } from "react-native"
import { Button, Text, TextInput } from "react-native-paper"
import { ICategory } from "../../../interfaces/returnObjects/ICategory"
import { useUserId } from "../../../state/useUserId"
import { createCategories } from "../../../senders/categoriesSenders"
import {formStyles} from "../../../styles/formStyles";
import {useNavigation} from "@react-navigation/native";

export default function CreateCategory() {
  const { userId } = useUserId()
  const navigation = useNavigation()
  const { values, handleChange } = useCreateCategoryForm()
  const currentMonth = new Date(Date.now()).getMonth()

  const onSubmit = () => {
    const body: ICategory = {
      name: values.name.toLowerCase(),
      amount: values.amount,
      currentMonth: currentMonth,
      spent: 0,
      userId: userId,
      due: values.due?.toLowerCase(),
      frequency: values.frequency?.toLowerCase(),
    }
    try {
      createCategories(body)
        .then(() => {
          Alert.alert("Success", "Category created successfully")
          //@ts-ignore
          navigation.reset({index: 0, routes:[{name: "Categories"}]})
          // @ts-ignore
          navigation.navigate("Categories")
        })
        .catch(() => {
          return new Error("Month creation unsuccessful")
        })
    } catch (e: any) {
      console.info(e.message, e.stack)
    }
  }

  return (
    <View style={formStyles.formContainer}>
      <Text style={{ color: "#D7D0CF", marginTop: 10, fontSize: 20 }}>
        Create new category
      </Text>
      <TextInput
        style={{ marginHorizontal: 50, marginTop: 20 }}
        label="Category name"
        mode="flat"
        onChangeText={text => handleChange("name", text)}
        value={values.name}
      />
      <TextInput
        style={{ marginHorizontal: 50, marginTop: 20 }}
        label="Category Budget"
        mode="flat"
        keyboardType={"numeric"}
        onChangeText={text => handleChange("amount", text)}
        value={values.amount.toString()}
      />
      <TextInput
        style={{ marginHorizontal: 50, marginTop: 20 }}
        label="Due date"
        mode="flat"
        onChangeText={text => handleChange("due", text)}
        value={values.due}
      />
      <TextInput
        style={{ marginHorizontal: 50, marginTop: 20 }}
        label="Bill Frequency"
        mode="flat"
        onChangeText={text => handleChange("frequency", text)}
        value={values.frequency}
      />
      <Button
        mode="outlined"
        style={{ marginHorizontal: 50, marginTop: 20 }}
        onPress={onSubmit}
      >
        Submit
      </Button>
    </View>
  )
}
