import Auth0 from "react-native-auth0"
import jwt_decode from "jwt-decode"
import { Alert } from "react-native"
import { KeyedMutator } from "swr"

const auth0 = new Auth0({
  domain: "dev-t9psb4xf.us.auth0.com",
  clientId: "boSbIqmJYhoX8fOnocfL2em0UdZD9X5l",
})

export const loginUser = (setUser: KeyedMutator<any>) => {
  return auth0.webAuth
    .authorize({
      scope: "openid profile email",
    })
    .then((credentials: any) => {
      setUser(jwt_decode(credentials.idToken))
    })
    .catch((error: any) => console.log(error))
}

export const logoutUser = (setUser: KeyedMutator<any>, navigation: any) => {
  auth0.webAuth
    .clearSession({})
    .then(() => {
      setUser("")
      Alert.alert("Logged out")
    })
    .then(() => {
      navigation.navigate("Home")
    })
    .catch(() => {
      console.log("Logged out failed")
    })
}
