export interface IMonth {
  _id?: string
  name: string
  userId: string
  income: number
  expenses: number
  year: number
}
