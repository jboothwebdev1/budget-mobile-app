import useSWR from "swr"
import {ICategory} from "../interfaces/returnObjects/ICategory"
import {api} from "../utils/api/api"
import {fetcher} from "../utils/fetcher/fetcher"
import {defaultHeaders} from "../utils/headers"

export function useGetAllCategories(userId: string) {
  const { data, error, mutate} = useSWR<Array<ICategory>>(
    `${api}/category/${userId}`,
    fetcher,
  )
  return {
    categories: data || [],
    categoriesLoading: !data && !error,
    updateCategories: mutate,
    categoriesError: error,
  }
}

export async function createCategories(item: ICategory) {
  const request = await fetch(`${api}/category`, {
    method: "POST",
    headers: defaultHeaders,
    body: JSON.stringify(item),
  })
  return request
}

export async function updateCategory(item: ICategory) {
  const request = await fetch(`${api}/category`, {
    method: "PATCH",
    headers: defaultHeaders,
    body: JSON.stringify(item),
  })
  return request
}

export async function deleteCategory(item: ICategory) {
  const request = await fetch(`${api}/category`, {
    method: "DELETE",
    headers: defaultHeaders,
    body: JSON.stringify(item),
  })
  return request
}

export async function updateCategoryOnNewMonth(month: any) {
  const request = await fetch(`${api}/category/${month}`, {
    method: "PATCH",
    headers: defaultHeaders,
  })
}
