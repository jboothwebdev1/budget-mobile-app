import { useState } from "react"
import { List } from "react-native-paper"

import { ICategory } from "../../interfaces/returnObjects/ICategory"
import { Alert, View } from "react-native"
import { useNavigation } from "@react-navigation/native"
import { deleteCategory } from "../../senders/categoriesSenders"
import {useGetTransactionsByMonth} from "../../senders/transactionSenders";

export default function CategoryCard(props: any) {
  const category: ICategory = props.category
  const updateCategories = props.updateCategories

  const [expanded, setExpanded] = useState<boolean>(false)
  const navigation = useNavigation()

  const handlePress = () => {
    setExpanded(!expanded)
  }

  const onEditCategory = (category: ICategory) => {
    // @ts-ignore
    navigation.navigate("EditCategory", { category: category, update: updateCategories})
  }

  const onDeleteCategory = () => {
    try {
      deleteCategory(category).then(() => {
        Alert.alert("Success", "Category deleted successfully")
        updateCategories()
      })
    } catch (e: any) {
      console.warn(e.message, e.stack)
      Alert.alert(
        "Unsuccessful",
        "Category deletion failed. Please try again" + "n",
      )
    }
  }

  return (
    <View>
      <List.Section
        style={{
          backgroundColor: "#2C2F44",
        }}
      >
        <List.Accordion
          title={category.name}
          style={{
            backgroundColor: "#2C2F44",
          }}
          titleStyle={{
            fontSize: 24,
            fontWeight: "bold",
          }}
          onPress={handlePress}
        >
          <List.Item
            title={"Budget: $" + category.amount.toFixed(2)}
            titleStyle={{
              color: "#56FF2C",
              fontWeight: "bold",
              marginLeft: 20,
            }}
          />
          <List.Item
            title={"Spent: $" + category.spent.toFixed(2)}
            titleStyle={{
              color: "#FF4341",
              fontWeight: "bold",
              marginLeft: 20,
            }}
          />
          <List.Item
            title={"Due date: " + category.due}
            titleStyle={{ marginLeft: 20 }}
          />
          <List.Item
            title={"Frequency: " + category.frequency}
            titleStyle={{ marginLeft: 20 }}
          />
          <View style={{ flexDirection: "row", justifyContent: "center" }}>
            <List.Item
              title=""
              onPress={() => {
                onEditCategory(category)
              }}
              left={props => (
                <List.Icon
                  {...props}
                  color="#33A2F5"
                  icon="circle-edit-outline"
                />
              )}
            />
            <List.Item
              title=""
              onPress={() => {onDeleteCategory()}}
              right={props => (
                <List.Icon
                  {...props}
                  color="#EE1869"
                  icon="delete-outline"
                />
              )}
            />
          </View>
        </List.Accordion>
      </List.Section>
    </View>
  )
}
