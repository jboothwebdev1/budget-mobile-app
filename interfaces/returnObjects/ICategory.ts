export interface ICategory {
  _id?: string
  name: string
  userId: string
  amount: number
  spent: number
  currentMonth: number
  due?: string
  frequency?: string
}
