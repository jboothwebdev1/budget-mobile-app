import * as React from "react"
import { IMonth } from "../../interfaces/IMonth"
import { Card, ProgressBar, Title } from "react-native-paper"
export default function MonthCard(props: any) {
  const month: IMonth = props.month
  const difference: number = parseFloat(
    (month.expenses / month.income).toFixed(1),
  )
  let color = "#19FF2C"
  if (difference > 0.5 && difference < 0.8) {
    color = "#FFF423"
  } else if (difference >= 0.8) {
    color = "#FF4341"
  }
  return (
    <Card style={{borderBottomWidth: 2, borderColor: "#BBB" ,marginTop: 4, backgroundColor: "transparent"}}>
      <Card.Title title={month.name} />
      <Card.Content>
        <Title style={{ color: "#0AFF4F"}}>Budget: ${month.income.toFixed(2)}</Title>
        <Title style={{ color: "#FF4341"}}>Total Spent: ${month.expenses.toFixed(2)}</Title>
        <ProgressBar
          style={{ marginTop: 10 }}
          progress={difference}
          color={color}
        />
      </Card.Content>
    </Card>
  )
}
