import { useGetAllCategories } from "../../senders/categoriesSenders"
import { Text } from "react-native"

import CategoryContainer from "../containers/CategoryContainer"
import Loading from "../utils/Loading"

export default function Categories(props: any) {
  const userId: string = props.userId
  const { categories, categoriesLoading, updateCategories } = useGetAllCategories(userId)
  return (
    <>
      {categoriesLoading ? (
        <Loading />
      ) : (
        <CategoryContainer categories={categories} updateCategories={updateCategories}/>
      )}
    </>
  )
}
