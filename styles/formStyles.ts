import {StyleSheet} from "react-native";

export const formStyles = StyleSheet.create({
  formContainer: {
    height: "100%",
    width: "100%",
  },
  formTitle:  {
    color: "#D7D0CF",
    marginTop: 5,
    marginLeft: 10,
    fontSize: 20
  },
  formInput: {
    marginHorizontal: 50,
    marginTop: 10
  },
  formButton: {
    marginHorizontal: 50,
    marginTop: 20
  },
  formPreview: {
    fontSize: 15,
    marginTop: 10,
    paddingHorizontal: 70,
  }
})