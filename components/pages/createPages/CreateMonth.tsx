import { useUserId } from "../../../state/useUserId"
import { useState } from "react"
import { Alert, View } from "react-native"
import MonthSelectionMenu from "../../menus/MonthSelectionMenu"
import YearSelectionMenu from "../../menus/YearSelectionMenu"
import { TextInput, Button, Text, Divider } from "react-native-paper"
import { IMonth } from "../../../interfaces/IMonth"
import { createMonth } from "../../../senders/monthSenders"
import {useNavigation} from "@react-navigation/native";

export default function CreateMonth() {
  const { userId } = useUserId()
  const navigation = useNavigation()
  const [month, setMonth] = useState<string>("")
  const [year, setYear] = useState<number>(0)
  const [income, setIncome] = useState<string>("0.00")

  const onSubmit = () => {
    const item: IMonth = {
      expenses: 0,
      income: parseFloat(income),
      name: month,
      userId: userId,
      year: year,
    }
    try {
      createMonth(item)
        .then(() => {
          Alert.alert("Success", "Month create successfully")
          // @ts-ignore
          navigation.reset({index: 0 , routes: [{name: "Month"}]})
          // @ts-ignore
          navigation.navigate("Month")
        })
        .catch(() => {
          return new Error("Month creation unsuccessful")
        })
    } catch (e: any) {
      console.info(e.message, e.stack)
    }
  }

  return (
    <View
      style={{
        width: "100%",
        flexDirection: "column",
        justifyContent: "center",
      }}
    >
      <Text style={{ color: "#D7D0CF", marginTop: 10, fontSize: 20 }}>
        Create month
      </Text>
      <MonthSelectionMenu setMonth={setMonth} />
      <YearSelectionMenu setYear={setYear} />
      <TextInput
        style={{ marginHorizontal: 50, marginTop: 20 }}
        label="Income"
        mode="flat"
        keyboardType="numeric"
        onChangeText={setIncome}
        value={income.toString()}
      />
      <Text
        style={{
          fontSize: 20,
          marginVertical: 20,
          paddingHorizontal: 50,
        }}
      >
        Month preview:
      </Text>
      <Text style={{ fontSize: 20, paddingHorizontal: 70 }}>
        Month: {month}
      </Text>
      <Text style={{ fontSize: 20, paddingHorizontal: 70, marginTop: 10 }}>
        Year: {year}
      </Text>
      <Text style={{ fontSize: 20, paddingHorizontal: 70, marginTop: 10 }}>
        Income: $ {income}
      </Text>
      <Button
        mode="outlined"
        style={{ marginHorizontal: 50, marginTop: 20 }}
        onPress={onSubmit}
      >
        Submit
      </Button>
    </View>
  )
}
