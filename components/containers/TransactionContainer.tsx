import { FlatList, View } from "react-native"
import { ITransactions } from "../../interfaces/ITransactions"
import TransactionCard from "../transactionComponents/TransactionCard"
import {updateTransaction} from "../../senders/transactionSenders";
type TCProps = {
  transactions: Array<ITransactions>
  updateTransactions: Function
}

export default function TransactionContainer({ transactions, updateTransactions }: TCProps) {
  return (
    <View>
      <FlatList
        data={transactions}
        renderItem={({ item }) => <TransactionCard transaction={item} updateTransactions={updateTransactions} />}
      />
    </View>
  )
}