import {useState} from "react"
import {ICategory} from "../../interfaces/returnObjects/ICategory";

type editCategory = {
  name: string,
  amount: number,
  spent: number,
  due?: string,
  frequency?: string
}

export default function useEditCategoryForm(initialState: ICategory){
  const [values, setValues] = useState<editCategory>(initialState)

  const handleChange = (key: string, value: string | number) => {
    setValues(values => ({...values, [key]: value }))
  }

  return {
    values,
      handleChange
  }
}