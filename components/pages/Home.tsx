import { StyleSheet, Text, TouchableOpacity, View } from "react-native"
import { strings } from "../../utils/strings/appDiscription"
import Auth0 from "react-native-auth0"
import { loginUser } from "../../senders/userSender"
import {useCallback, useEffect, useState} from "react";
import * as SplashScreen from 'expo-splash-screen';

const auth0 = new Auth0({
  domain: "dev-t9psb4xf.us.auth0.com",
  clientId: "boSbIqmJYhoX8fOnocfL2em0UdZD9X5l",
})

export default function Home({ setUser, navigation }: any) {
  const onLogin = () => {
    loginUser(setUser).then(() => navigation.navigate("Budget"))
  }
  const [appIsReady, setAppIsReady] = useState(false);

  useEffect(() => {
    async function prepare() {
      try {
        // Keep the splash screen visible while we fetch resources
        await SplashScreen.preventAutoHideAsync();
        // Pre-load fonts, make any API calls you need to do here
      } catch (e) {
        console.warn(e);
      } finally {
        // Tell the application to render
        setAppIsReady(true);
      }
    }
    prepare();
  }, [])

  const onLayoutRootView = useCallback(async () => {
    if (appIsReady) {
      // This tells the splash screen to hide immediately! If we call this after
      // `setAppIsReady`, then we may see a blank screen while the app is
      // loading its initial state and rendering its first pixels. So instead,
      // we hide the splash screen once we know the root view has already
      // performed layout.
      await SplashScreen.hideAsync();
    }
  }, [appIsReady]);

  if (!appIsReady) {
    return null;
  }

  return (
    <View style={styles.container} onLayout={onLayoutRootView}>
      <View style={styles.titleContainer}>
        <Text style={styles.appTitle}> onBudget </Text>
        <Text style={styles.appDiscription}>{strings.appDiscription}</Text>
      </View>
      <View style={styles.loginContainer}>
        <Text style={styles.loginStatement}>{strings.loginStatement}</Text>
        <TouchableOpacity
          style={styles.buttonContainer}
          onPress={onLogin}
        >
          <Text style={styles.buttonText}>Login</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#2A2B42",
    justifyContent: "center",
    alignItems: "center",
  },
  appDiscription: {
    fontSize: 16,
    fontWeight: "600",
    color: "#AAAAAA",
  },
  appTitle: {
    fontSize: 40,
    fontWeight: "bold",
    color: "#AAAAAA",
  },
  buttonContainer: {
    backgroundColor: "#0E7EFF",
    marginTop: 10,
    padding: 10,
    alignItems: "center",
  },
  buttonText: {
    fontSize: 16,
    fontWeight: "bold",
    color: "#FFFFFF",
  },

  loginContainer: {
    flex: 1,
    width: "80%",
  },
  loginStatement: {
    color: "#AAAAAA",
    fontWeight: "600",
    fontSize: 16,
  },
  titleContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
})
