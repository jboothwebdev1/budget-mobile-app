import { Button, Menu, Provider } from "react-native-paper"
import { View } from "react-native"
import { useState } from "react"
import { MONTHS } from "../../utils/months/months"
import { IMonth } from "../../interfaces/IMonth"
import { createMonth } from "../../senders/monthSenders"

const months = MONTHS

type propTypes = {
  setMonth: Function
}

export default function MonthSelectionMenu({ setMonth }: propTypes) {
  const [visible, setVisible] = useState(false)

  const openMenu = () => setVisible(true)

  const closeMenu = () => setVisible(false)

  return (
    <View style={{ padding: 50 }}>
      <Menu
        visible={visible}
        onDismiss={closeMenu}
        anchor={
          <Button
            mode="contained"
            color="#bbb"
            onPress={openMenu}
          >
            Select month
          </Button>
        }
      >
        {months.map(month => {
          return (
            <Menu.Item
              key={month.index}
              title={`${month.index} - ${month.name}`}
              onPress={() => {
                setMonth(`${month.index} - ${month.name}`)
                closeMenu()
              }}
            />
          )
        })}
      </Menu>
    </View>
  )
}
