import { ITransactions } from "../../interfaces/ITransactions"
import { useState } from "react"

const initialState: ITransactions = {
  category: "",
  month: "",
  spent: 0,
  userId: "",
  year: 0,
}

export default function useCreateTransactionForm() {
  const [values, setValues] = useState<ITransactions>(initialState)

  const handleChange = (key: string, value: string | number) => {
    setValues(values => ({ ...values, [key]: value }))
  }
  return {
    values,
    handleChange,
  }
}
