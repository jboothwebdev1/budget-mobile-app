export interface ITransactions {
  _id?: string
  category: string
  userId: string
  month: string
  spent: number
  year: number
}
