import * as React from "react"
import AppContainer from "../containers/AppContainer"
import { View } from "react-native"
import MonthMenu from "../menus/MonthMenu"
import { StatusBar } from "expo-status-bar"

export default function Month({ userId }: any) {
  return (
    <View>
      <StatusBar style="light" />
      <AppContainer userId={userId} />
    </View>
  )
}
