import {
  useGetAllTransactions,
  useGetTransactionsByMonth,
} from "../../senders/transactionSenders"
import TransactionContainer from "../containers/TransactionContainer"
import Loading from "../utils/Loading"

type currentMonth = {
  name: string
}

export default function Transactions(props: any) {
  const userId: string = props.userId
  const month: currentMonth = props.month

  const { transactions, transactionsLoading , updateTransactions} = useGetTransactionsByMonth(
    month,
    userId,
  )
  return (
    <>
      {transactionsLoading ? (
        <Loading />
      ) : (
        <TransactionContainer transactions={transactions} updateTransactions={updateTransactions} />
      )}
    </>
  )
}
