import { AppState } from "react-native"
import { Provider as PaperProvider, DarkTheme } from "react-native-paper"
import { createNativeStackNavigator } from "@react-navigation/native-stack"
import { NavigationContainer } from "@react-navigation/native"

// Pages
import Home from "./components/pages/Home"
import Budget from "./components/pages/Budget"
import CreateMonth from "./components/pages/createPages/CreateMonth"
import { useState } from "react"
import { initialUser, IUser } from "./interfaces/IUser"
import { SWRConfig } from "swr"
import CreateCategory from "./components/pages/createPages/CreateCategory"
import CreateTransaction from "./components/pages/createPages/CreateTransaction"
import EditTransactionForm from "./components/forms/EditTransactionForm"
import EditCategoryForm from "./components/forms/EditCategoryForm"
import EditMonthForm from "./components/forms/EditMonthForm"

type RootStackParamList = {
  Home: undefined
}

const theme = {
  ...DarkTheme,
  animation: {
    scale: 1.0,
  },
  colors: {
    ...DarkTheme.colors,
    background: "#0F1724",
  },
  dark: true,
  roundness: 2,
  version: 3,
}

const Stack = createNativeStackNavigator<RootStackParamList>()

export default function App() {
  const [user, setUser] = useState<IUser>(initialUser)


  // @ts-ignore
  return (
    <SWRConfig
      value={{
        provider: () => new Map(),
        isOnline() {
          return true
        },
        isVisible() {
          return true
        },
        initFocus(callback: any) {
          let appState = AppState.currentState

          const onAppStateChange = (nextAppState: any) => {
            /* If it's resuming from background or inactive mode to active one */
            if (
              appState.match(/inactive|background/) &&
              nextAppState === "active"
            ) {
              callback()
            }
            appState = nextAppState
          }

          // Subscribe to the app state change events
          const subscription = AppState.addEventListener(
            "change",
            onAppStateChange,
          )

          return () => {
            subscription.remove()
          }
        },
      }}
    >
      <PaperProvider theme={theme}>
        <NavigationContainer theme={theme}>
          <Stack.Navigator
            initialRouteName={"Home"}
            screenOptions={{ headerShown: false,   headerStyle: {
                backgroundColor: "#434365",
              },
              headerTintColor: "#fff",}}
          >
            <Stack.Screen name="Home"
                          options={{

                            headerStyle: {
                            backgroundColor: "#434365",
                          },
                            headerTintColor: "#fff",
                          }}
            >
              {props => (
                <Home
                  {...props}
                  setUser={setUser}
                />
              )}
            </Stack.Screen>
            <Stack.Screen
              name="Budget"
              options={{}}
            >
              {props => (
                <Budget
                  {...props}
                  user={user}
                />
              )}
            </Stack.Screen>
            <Stack.Screen
              name="CreateMonth"
              options={{
                title: "Create month",
                headerShown: true,
                headerStyle: {
                  backgroundColor: "#434365",
                },
                headerTintColor: "#fff",
              }}
            >
              {props => <CreateMonth />}
            </Stack.Screen>

            <Stack.Screen
              name="CreateCategory"
              options={{
                title: "Create month",
                headerShown: true,
                headerStyle: {
                  backgroundColor: "#434365",
                },
                headerTintColor: "#fff",
              }}
            >
              {props => <CreateCategory />}
            </Stack.Screen>

            <Stack.Screen
              name="CreateTransaction"
              options={{
                title: "Create month",
                headerShown: true,
                headerStyle: {
                  backgroundColor: "#434365",
                },
                headerTintColor: "#fff",
              }}
            >
              {props => <CreateTransaction />}
            </Stack.Screen>

            <Stack.Screen
              name="EditTransaction"
              options={{
                title: "Create month",
                headerShown: true,
                headerStyle: {
                  backgroundColor: "#434365",
                },
                headerTintColor: "#fff",
              }}
            >
              {props => <EditTransactionForm route={props.route} />}
            </Stack.Screen>
            <Stack.Screen
              name="EditCategory"
              options={{
                title: "Create month",
                headerShown: true,
                headerStyle: {
                  backgroundColor: "#434365",
                },
                headerTintColor: "#fff",
              }}
            >
              {props => <EditCategoryForm route={props.route} />}
            </Stack.Screen>

            <Stack.Screen
              name="EditMonth"
              options={{
                title: "Create month",
                headerShown: true,
                headerStyle: {
                  backgroundColor: "#434365",
                },
                headerTintColor: "#fff",
              }}
            >
              {props => <EditMonthForm route={props.route} />}
            </Stack.Screen>
          </Stack.Navigator>
        </NavigationContainer>
      </PaperProvider>
    </SWRConfig>
  )
}
