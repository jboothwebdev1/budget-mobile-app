import { ICategory } from "../../interfaces/returnObjects/ICategory"
import { FlatList, View } from "react-native"
import CategoryCard from "../categoryComponents/CategoryCard"

export default function CategoryContainer(props: any) {
  const categories: Array<ICategory> = props.categories
  const updateCategories = props.updateCategories

  return (
    <View>
      <FlatList
        data={categories}
        renderItem={({ item }) => <CategoryCard category={item} updateCategories={updateCategories}/>}
      />
    </View>
  )
}
