import { Menu, Divider, Button, Title } from "react-native-paper"
import { useState } from "react"
import { useNavigation } from "@react-navigation/native"
import { logoutUser } from "../../senders/userSender"
import { useUserId } from "../../state/useUserId"

type propType = {
  userId: string
}

export default function MonthMenu({ userId }: propType) {
  const { setUserId } = useUserId()
  const [visible, setVisible] = useState(false)
  const navigation = useNavigation()

  const openMenu = () => {
    setVisible(true)
  }

  const closeMenu = () => {
    setVisible(false)
  }

  return (
    <Menu
      visible={visible}
      onDismiss={closeMenu}
      anchor={
        <Button
          icon="menu"
          onPress={openMenu}
        />
      }
    >
      <Title> Create </Title>
      <Divider />
      <Menu.Item
        onPress={() => {
          // @ts-ignore
          navigation.navigate("CreateMonth")
        }}
        title="Month"
      />

      <Menu.Item
        onPress={() => {
          // @ts-ignore
          navigation.navigate("CreateCategory")
        }}
        title="Category"
      />

      <Menu.Item
        onPress={() => {
          // @ts-ignore
          navigation.navigate("CreateTransaction")
        }}
        title="Transactions"
      />

      <Title> User </Title>
      <Divider />
      <Menu.Item
        onPress={() => {
          logoutUser(setUserId, navigation)
        }}
        title="Logout"
      />
    </Menu>
  )
}
