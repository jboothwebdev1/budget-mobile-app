import { View } from "react-native"
import { ActivityIndicator, Text } from "react-native-paper"

export default function Loading() {
  return (
    <View
      style={{
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
        height: "100%",
      }}
    >
      <Text style={{ fontSize: 50 }}>Loading</Text>
      <ActivityIndicator
        color={"#33A2F5"}
        size="large"
        animating={true}
      />
    </View>
  )
}
