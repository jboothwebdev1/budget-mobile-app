import { ITransactions } from "../../interfaces/ITransactions"
import { Alert, View } from "react-native"
import { List } from "react-native-paper"
import { deleteTransaction } from "../../senders/transactionSenders"
import { useNavigation } from "@react-navigation/native"

type props = {
  transaction: ITransactions
  updateTransactions: Function
}
export default function TransactionCard({ transaction, updateTransactions }: props) {
  const navigation = useNavigation()
  const formatedMonth = transaction.month.split(" ").slice(2)

  const onEditTransaction = (item: ITransactions) => {
    // @ts-ignore
    navigation.navigate("EditTransaction", { transaction: item, update: updateTransactions})
  }

  const onDeleteTransaction = (item: ITransactions) => {
      try {
        deleteTransaction(item).then(() => {
          Alert.alert("Success", "Transaction deleted successfully")
          updateTransactions()
          // @ts-ignore
          navigation.reset({index: 0, routes:[{name: "Transactions"}]})
        })
      } catch (e: any) {
        console.log(e.message, e.stack)
        Alert.alert(
          "Unsuccessful",
          "Transaction deletion failed. Please try again.",
        )
      }
  }

  return (
    <View>
      <List.Section style={{ backgroundColor: "#2C2F44" }}>
        <List.Accordion
          title={`${transaction.category}: ${formatedMonth}-${transaction.year}`}
          style={{ backgroundColor: "#2C2F44" }}
          titleStyle={{ fontSize: 24, fontWeight: "bold" }}
        >
          <List.Item
            title={`Amount paid: $${transaction.spent.toFixed(2)}`}
            titleStyle={{
              color: "#FF4341",
              fontWeight: "bold",
              marginLeft: 20,
            }}
          />
          <View style={{ flexDirection: "row", justifyContent: "center" }}>
            <List.Item
              title=""
              onPress={() => {
                onEditTransaction(transaction)
              }}
              left={props => (
                <List.Icon
                  {...props}
                  color="#33A2F5"
                  icon="circle-edit-outline"
                />
              )}
            />
            <List.Item
              title=""
              onPress={() => {
                onDeleteTransaction(transaction)
              }}
              right={props => (
                <List.Icon
                  {...props}
                  color="#EE1869"
                  icon="delete-outline"
                />
              )}
            />
          </View>
        </List.Accordion>
      </List.Section>
    </View>
  )
}
